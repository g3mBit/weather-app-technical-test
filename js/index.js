/** @jsx React.DOM */

/* 
* Defining API Key Variable
*/
var key = "bc95b89f3c240ef42330093ff07edeef";

/** 
* Initializing Variables: These variables will be updated with data from the API
**/

var WeatherApp = React.createClass({ 
  getInitialState: function () {            
   return {
      location: '', 
      weather_details:  '',
      temperature:  '',
      icon:     ''
    };
  },

/**
* Getting Data from Weather API using 'Metric' as Units
**/ 
  getData: function (location, lat, lon) {
     var data;      
     data = $.get('https://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + lon + '&APPID=' + key + "&units=metric&mode=xml");    
    return data;
  },
  
/**
* Convert(): This function will convert the data in XML to JSON 
**/  

  convert: function(data){
    xml = new XMLSerializer().serializeToString(data.documentElement)
    
    var options = {
        attributeNamePrefix : "",
        attrNodeName: false,
        textNodeName : "#text",
        ignoreAttributes : false,
        ignoreNameSpace : false,
        allowBooleanAttributes : false,
        parseNodeValue : true,
        parseAttributeValue : false,
        trimValues: true,
        decodeHTMLchar: true,
        cdataTagName: "__cdata", //default is 'false'
        cdataPositionChar: "\\c",
    };
    var result = parser.validate(xml);
    
	/* If the conversion is successful, we will assignt the values to the variables */
	
	if(result=== true){
     
      var data = parser.parse(xml, options);
      data =data.current
      this.setState({
        lat:      data.city.coord.lat,
        lon:      data.city.coord.lon,
        weather_details:  data.weather.value ,
        location: data.city.name,
        temperature: data.temperature.value + "°C",
        icon:     'https://openweathermap.org/img/w/' + data.weather.icon + '.png' 
    })   }
  },
  
  
/**
* updateState()
**/ 

  updateState: function (locationName, lat, lon) {
    this.getData(locationName, lat, lon)
      .then(function(data) {
        this.convert(data);
    }.bind(this));
  },

    
/**
*  geolocationSearch(): This function runs in the first load(componentDidMount) and when we click on the Get Weather Information' button
**/
 
 geolocationSearch: function () {    
   
    var success = function (location) {
	
	  /* if/else: For the location, the first time that the call to the API is done, we will store the lon/lat into local variables to be used for future request */
	  if (window.localStorage.getItem('user_location_latitude') == null && window.localStorage.getItem('user_location_longitude') == null) {
		  var lat = location.coords.latitude;
		  var lon = location.coords.longitude;      
		  
		  window.localStorage.setItem('user_location_latitude', lat);
		  window.localStorage.setItem('user_location_longitude', lon);
	  } 
	  else { /* If not the first call, we will use the values from the local variables stored */
		  lat = window.localStorage.getItem('user_location_latitude')
		  lon = window.localStorage.getItem('user_location_longitude')
	  }
	  
      this.updateState(null, lat, lon);
    }.bind(this);
  
   
    var error = function (error) {
      if (error.message == 'User does not allow Geolocation')
      {
        alert('Please enable Location Services');
      }
    };
  
    navigator.geolocation.getCurrentPosition(success, error);
  },
  
   
/**
*  componentDidMount(): We are getting the coordinates when the component first loads (mounts)
**/
  componentDidMount: function () {    
    this.geolocationSearch();   
  },
  
  /**
   * Render the Weather App
   **/
  render: function() {
    return (
      <div id="app">           
        <div className="button_container">
          <button type="button" onClick={this.geolocationSearch}>
            Get Weather Information 
          </button>
        </div>
        <WeatherInformation location={this.state.location} weather={this.state.weather_details} temperature={this.state.temperature} icon={this.state.icon} /> 
      </div>
    );
  }
  
});

var WeatherInformation = React.createClass({  
  render: function() {    
    const imgWeather = this.props.icon; 
    let imgWeatherIcon ;
   
    /*If not image is retuned, we won't display the img tag */
    if (imgWeather != "")
    {
      imgWeatherIcon = <img border="0" src={this.props.icon}  />;
    }
         
    return (  
      <div className="weather">
        <h1>{this.props.location}</h1>
        <h2>{this.props.temperature}{imgWeatherIcon} </h2>
        <p>{this.props.weather}</p>
      </div>
    )
  }
  
});

/**
* Display Weather Information on Page
**/

React.renderComponent(
  <WeatherApp />,
  document.getElementById('weather_info')
);