# Weather App - Technical Test
To Run the project, please follow the steps below

1. Install yarn. Please see reference here: https://yarnpkg.com/en/docs/install#windows-stable

2. Once yarn is installed, open the project directory path in a command prompt windows and run the following commands 'yarn install' and then 'yarn start'

3. Once the 'yarn start' command was run, you will see a line with the following: " Project is running at http://localhost:8080/ ", open the URL in a browser to see the project run.

